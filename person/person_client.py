#! /usr/bin/env python
from __future__ import print_function
import rospy
# Brings in the SimpleActionClient
import actionlib


# Brings in the messages used by the speech action, including the
# goal message and the result message.
import person.msg

def person_client(y):
    # Creates the SimpleActionClient, passing the type of the action
    # (SpeechAction) to the constructor.
    client = actionlib.SimpleActionClient('person', person.msg.message1Action)
    
    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()
    
 
    # Creates a goal to send to the action server.
    
    goal = person.msg.message1Goal(text=y)
   
    # Sends the goal to the action server.
    client.send_goal(goal)
    

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result() 

if __name__ == '__main__':
    while 1:
       
            try:
		y=raw_input("enter the name of the person")
 		print (y)
        	# Initializes a rospy node so that the SimpleActionClient can
        	# publish and subscribe over ROS.
        	rospy.init_node('person_')
        	result = person_client(y)
		print("name:",y)
		if result.sequence==0:
			print("no details")
		else:
        		print("Result:",result.sequence)
    	    except rospy.ROSInterruptException:
        	print("program interrupted before completion", file=sys.stderr)


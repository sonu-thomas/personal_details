#! /usr/bin/env python

import rospy

import actionlib

import person.msg



class message1Action(object):
    # create messages that are used to publish feedback/result
    _feedback = person.msg.message1Feedback()
    _result = person.msg.message1Result()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, person.msg.message1Action, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
      
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # publish info to the console for the user
        rospy.loginfo('performing analysis  %s',goal.text)
        x=goal.text
       
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
           
            # publish the feedback
	personal_details={'sonu':20,'sruthy':21,'meera':22,'archana':23}
	for i in personal_details:
	    flag=0
            if i==x:
		flag=1
		s=personal_details[i]
		break
	if flag==1:
               	self._feedback.sequence=s
               	self._as.publish_feedback(self._feedback)
        else:
		self._feedback.sequence=0
                self._as.publish_feedback(self._feedback)


            # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
        r.sleep()
          
        if success:
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('person')
    server = message1Action(rospy.get_name())
    rospy.spin()
